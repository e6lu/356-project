# US Election Group 17
Members : Eugene Lu and Daniel van den Hoven

## Pre-Requisites

1. `pip install mysql-connector-python`
2. `pip install python-dotenv`
3. Update .env to enter your credentials in where `DB_USER` represents username for Marmoset4 and `DB_PASS` represents password to login to Marmoset4 and `DB_NAME` represents the name of the database you wish to connect to.

## DB Tools
make sure these commands are run from the directory root
#### To create all the tables and import the data:
`python3 db/db_init.py`

#### To reset the database:
`python3 db/db_reset.py`

## Running the CLI
`python3 CLI/client.py`
The CLI is an infinite loop until the command quit is entered. A good place to start to understand
the CLI is to run `help` or to view our video.

## Testing
Test cases are shown under `TestCases` which represent the tests run to maintain integrity of our client. 
Full details of test plan can be viewed under the doc.
