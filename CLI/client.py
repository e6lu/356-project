#!/usr/bin/python3

import os
import dotenv

import mysql.connector as mysql


# Removes ( ) ' , characters from passed in string and returns it
# Used to neatly format  list
def trim_string (name: str):


    name = str(name)
    name = name.replace('(','')
    name = name.replace(')','')
    name = name.replace(',','')
    name = name.replace('\'','')

    return name

# Removes leading/trailing whitespace from all elements in a list and returns updated list
def trim(old_list : list):

    new_list = []

    for element in old_list:

        element = str(element)
        element = element.strip()
        element = trim_string(element)
        new_list.append(element)

    return new_list

# Hardcoded list of available commands for when user runs help command

def generate_commands():

    commands = ("state_votes", "add_user", "find_user", "county_results", "group_county_by", "get_tweet_candidate",
                "count_tweet_region", "update_poll_count", "update_state_count", "insert_tweet",
                "add_annotation_county","view_annotation_county", "help", "quit")

    return commands

# Hardcoded list of available descriptions when running help command
def generate_descriptions():

    descriptions = ("Enter in a state to view the total number of votes in the 2020 election. Option \"All\" to view votes in descending order by state",
                    "Adds a new user to the database. Used for supporting a new addition to the database.",
                    "Used to find users with high follower counts by county",
                    "Prints out the final voting standings by party in a specific county",
                    "Ranks counties in a given state or across the nation in a specific category (ex. Covid Cases, Income)",
                    "Displays all tweets about a particular candidate above a certain like threshold",
                    "Displays the total amount of tweets about the election by region within a specific date range about a candidate",
                    "Updates the final vote count for a party. Used to update vote counts for parties as votes are being processed",
                    "Updates the total number of votes by state. Used to update voting data as votes are being processed",
                    "Inserts new tweet to the database by a specific user",
                    "Insert/Modify data annotations in a given county election race under each party",
                    "View previously insert data annotations on all parties in a given county",
                    "Prints out list of available commands and their options",
                    "Closes the CLI"
                    )

    return descriptions

# Database class : Used for interacting with the Database and performing queries
class Database:

        # Creates a blank table with attributes
        def __init__(self):


            self.command_list = generate_commands()
            self.command_description = generate_descriptions() # List of user command descriptions
            self.state_id = self.setup_stateid_table()


        # COMMAND SPECIFIC FUNCTIONS - THESE ARE THE FUNCTIONS THAT IMPLEMENT THE COMMANDS DIRECTLY

        # find_user command : Returns users that match location and follower criteria
        def find_user(self):

            state = input("Enter the state to search in : ")
            state = state.strip()
            county = input("Enter the county in {0} to search in or (All): ".format(state))
            county = county.strip()

            min_follower_count = input("Enter minimum follower count to query by : ")
            min_follower_count = min_follower_count.strip()
            max_follower_count = input("Enter maximum follower count to query by (leave blank for infinite) : ")
            max_follower_count = max_follower_count.strip()

            limit = input("All results or limit results (All/Limit) : ")
            limit = limit.strip()
            limit = limit.lower()
            limit_string = ""

            if limit == "limit":
                limit_string += self.limit_results()

            if county == "All":
                #Case 1a : State-wide search, No Upper limit given. Search across entire state for users that exceed min_follower_count
                if max_follower_count == "":
                    query = "select display_name, follower_count from user where follower_count >= {0} and " \
                            "county_id in (select id from county where state_id = (select id from state where name=\"{1}\")) order by follower_count desc {2};".format(min_follower_count, state, limit_string)

                #Case 1b: State-wide search, upper limit given. Search across entire state for users that fall within max, min follower range
                else:
                    query = "select display_name, follower_count from user where follower_count between {0} and {1} and " \
                            "county_id in (select id from county where state_id = (select id from state where name=\"{2}\")) order by follower_count desc {3};".format(
                        min_follower_count, max_follower_count, state, limit_string)
                #print(query)
                results = self.execute_query(query, False)

                if len(results) == 0:

                    print("No results returned for county {0} in the state of {1}".format(county, state))
                else:
                    counter = 1
                    for result in results:
                        print("{2:4}. Username : {0:30} Follower Count : {1}".format(result[0], result[1], counter))
                        counter += 1

                    return
                # Case 2a: County-wide search, No Upper follower limit given. Search for all users in the county that are at least the minimum
            if max_follower_count == "":
                query = "select display_name, follower_count from user where follower_count > {0} AND" \
                        " county_id = (select id from county where name = \"{1}\" and state_id = (select id from state where name = \"{2}\" )) order by follower_count desc {3};"\
                    .format( min_follower_count, county, state, limit_string)
            else:
                # Case 2b: County-wide search, Upper  follower limit given. Search for all users in the county that fall within follower range
                query = "select display_name, follower_count from user where follower_count between {0} and {1} AND" \
                        " county_id = (select id from county where name = \"{2}\" and state_id = (select id from state where name = \"{3}\" )) order by follower_count desc {4};".format(min_follower_count, max_follower_count,
                                                                                                                                                        county, state , limit_string)

           # print(query)
            results = self.execute_query(query, False)

            if len(results) == 0:

                print("No results returned for county {0} in the state of {1}".format(county, state))
            else:
                counter = 1
                for result in results:

                    print("{2:4}. Username : {0:30} Follower Count : {1}".format(result[0], result[1], counter))
                    counter += 1
            return
        # add_user command : Creates a user and adds their location and follower_count
        def add_user(self):

            county = input("Enter name of county that user resides in : ")
            county = county.strip()
            state = input("Enter name of state that {0} is in : ".format(county))
            state = state.strip()

            display = input("Enter username of the user : ")
            display = display.strip()

            followers = input("Enter their follower count : ")
            followers = followers.strip()

            identifier = self.get_new_user_id()+1 # Max current user id + 1

            county_id = self.get_county_id(county, state)

            query = "insert into user (id, county_id, display_name, follower_count) \n"
            query += "values({0},{1},\"{2}\",{3});".format(identifier, county_id, display, followers)
            # print(query)
            self.execute_query(query, True)

            # get_votes_state command - Returns the number of votes recorded in a specific state

        def get_votes_state(self):

            state = input("Enter state that you wish to filter by or All : ")
            state = state.strip()

            # Case 1 : User selects "All", list all state counts and sort in descending order by votes
            if state == "All":
                query = "select name, total_votes from state order by total_votes desc ;"
                results = self.execute_query(query, False)
                counter = 1

                for element in results:

                    if element[0] == "United States":
                        print("Total Votes in the United States : {0}".format(element[1]))
                    else:
                        print("{0:2}. State : {1:20} Total Votes : {2}".format(counter, element[0], element[1]))
                        counter += 1

                return
            # Case 2 : User selects a state. List state vote count for that state
            query = "select total_votes from state where name =\"{0}\"".format(state)

            results = self.execute_query(query, False)
            output = "'"
            for row in results:
                output = row[0]
            print("Number of votes from {0} is {1}".format(state, output))
            return

            # results county command - Prints the election results for a given county
        def results_county(self):

            state = input("Enter name of state that you wish to search in : ")
            state = state.strip()
            county = input("Enter name of county that you are interested in : ")
            county = county.strip()

            query = "select candidate, party, votes from county_candidate_statistics where " \
                        "county_id = (select id from county where name=\"{0}\" and state_id = ( select id from state " \
                        "where name = \"{1}\"));".format(county, state)

            results = self.execute_query(query, False)
                # print(query)
            if len(results) == 0:
                print("No results to show")
                return

            counter = 1
            print("Poll Rankings are as follows : ")

            for result in results:
                print("{0}. Candidate Name : {1:20}  Party Affiliation : {2}  Vote Count : {3}".format(counter,
                                                                                                           result[0],
                                                                                                           result[1],
                                                                                                           result[2]))
                counter += 1

        # group_county_by command : Sorts counties and ranks them based off a statistic in the list below.
        # supports rankings by state and rankings across the nation (All option)
        def group_county_by(self):

            state = input("Select state to filter results by or (All) :")
            state = state.strip()

            print("Statistics list is : \nCases, Deaths, Population, Men, Women, Hispanic, White, Black, Native, \n"
                  "Asian, Pacific, Voting Age, Income, Income per Capita, Poverty, Child Poverty, Professional, \n"
                  "Service, Office, Construction, Production, Drive, Carpool, Transit, Unemployed  \n"
                  "Walk, Work From Home, Employed, Private Work, Public Work, Self Employed, Family Work, \n")
            old_choice = input("Select statistic to query by : ")
            old_choice = old_choice.strip()
            choice = self.sanitize_categories(old_choice)

            limit = input("All results or limit results (All/Limit) : ")
            limit = limit.strip()
            limit = limit.lower()
            limit_string = ""

            if limit == "limit":
                limit_string += self.limit_results()

            order = input("Ascending or Descending Order (Asc/Desc) : ")
            order = order.strip()
            order = order.lower()
            order_string = ""

            if order == "asc":
                order_string = "order by {0} asc ".format(choice)
            else:
                order_string = "order by {0} desc ".format(choice)


            query = ""

            if state == "All": # Case 1 : Nationwide rankings

                query += "select name, {0}, state_id from county group by id ".format(choice)

            else: # Case 2 : Statewide rankings

                query += "select name, {0}, state_id from county where state_id = (select id from state where name = \"{1}\")  group by id ".format(choice, state)

            query += order_string
            query += limit_string
            query += ";"
            #print(query)
            results = self.execute_query(query, False)

            if len(results) == 0:
                print("No results returned")

            counter = 1
            for result in results:

                state_name = self.state_id[result[2]]
                print("{0:4}. County : {1:15} State : {2:15}  {3} : {4}".format(counter, result[0], state_name,  old_choice, result[1]))
                counter += 1

        # get_tweet_candidate command : returns all tweets above a certain like threshold about a candidate in a specific county
        def get_tweet_candidate(self):

            candidate_name = input("Enter the candidate name (Joe Biden/ Donald Trump) : ")
            candidate_name = candidate_name.strip()
            candidate_name = candidate_name.replace(" ","") #to JoeBiden or DonaldTrump

            state = input("Enter the name of the state : ")
            state = state.strip()
            likes = input("Input the minimum amount of likes to query by : ")
            likes = likes.strip()


            query = "select display_name, tweet, likes from user inner join tweet on tweet.user_id=user.id where likes >= {0} and hashtag = \"{1}\"" \
                    " and county_id in (select id from county where state_id = (select id from state where name = \"{2}\")) order by likes desc;" .format(likes, candidate_name, state)

            # print(query)
            results = self.execute_query(query, False)

            if len(results) == 0:
                print("No results were returned for the state of {0}".format(state))
                return
            print("Results for the state of {0}".format(state))

            for result in results:

                print("Username : {0:20} Tweet : {1:100} Likes : {2} ".format(result[0], result[1], result[2]))
            return

        # count_tweet_region command : Gets number of counts by region that is within a time period and exceeds the
        # like threshold
        def count_tweet_region(self):

            candidate_name = input("Enter the candidate name (Joe Biden/Donald Trump) : ")
            candidate_name = candidate_name.strip()
            candidate_name = candidate_name.replace(" ", "")

            state = input("Enter the name of the state : ")
            state = state.strip()

            likes = input("Input the minimum amount of likes to query by : ")
            likes = likes.strip()

            starting_date = input("Enter Start Date to filter by (YYYY-MM-DD): ")
            starting_date = starting_date.strip()
            starting_date += " 00:00:00"
            end_date = input("Enter End Date to filter by (YYYY-MM-DD): ")
            end_date = end_date.strip()
            end_date+=" 00:00:00"


            query = "select count(tweet) from tweet inner join user on tweet.user_id=user.id where likes >= {0} and hashtag = \"{1}\"" \
                    " and county_id in (select id from county where state_id = (select id from state where name = \"{2}\")) order by likes desc;".format(likes, candidate_name,  state )

            # print(query)
            results = self.execute_query(query, False)

            if len(results) == 0:
                print("No tweets in state {0} above {1} likes in this timeframe".format(state, likes))
                return

            for row in results:
                print("Number of tweets in state {0} above {1} likes within timeframe is {2}".format(state, likes, row[0]))
            return

        # update_poll_count command, updates party vote tally in a specific county
        def update_poll_count(self):

            county = input("Enter the county that you wish to update the poll result for : ")
            county = county.strip()
            state = input("Enter the state that {0} is in : ".format(county))
            state = state.strip()

            query = "select party, votes from county_candidate_statistics where " \
                        "county_id = (select id from county where name=\"{0}\" and state_id = ( select id from state " \
                        "where name = \"{1}\"));".format(county, state)

            results = self.execute_query(query, False)

            if len(results) == 0:
                print("No results to show. State/County pairing does not exist.")
                return

            counter = 1
            print("Poll Rankings in {0} are as follows : ".format(county))

            for result in results:
                print("Party Affiliation : {0:3}  Vote Count : {1}".format(result[0], result[1]))
                counter += 1

            party = input("Enter the party that you wish to modify : ")
            party = party.strip()

            votes = input("Input the number of votes that you wish to update it to : ")
            votes = votes.strip()

            query = "Update county_candidate_statistics \n"
            query += "Set votes = {0}\n".format(votes)
            query += "Where party = \"{0}\" and county_id = (select id from county where name = \"{1}\"" \
                         " and state_id = (select  id from state where name = \"{2}\"));".format(party, county, state)

            results = self.execute_query(query, True)

        # update_state_count : Updates the total number of votes in the state
        def update_state_count(self):

            state = input("Enter the state vote count that you wish to update : ")

            query = "select total_votes from state where name =\"{0}\"".format(state)

            results = self.execute_query(query, False)

            output = trim_string(results[0])

            for result in results:
                print("Number of votes from {0} is currently :  {1}".format(state, result[0]))

            value = input("Enter new value that you wish to update the state count to : ")

            query = "update state \n"
            query += "set total_votes = {0} \n".format(value)
            query += "where name = \"{0}\"".format(state)

            results = self.execute_query(query, True)

        # insert tweet command : Insert a tweet with either a Donald Trump or Joe Biden Hashtag
        def insert_tweet(self):
            username = input("Enter the username of this tweet : ")
            username = username.strip()

            tweet = input("Enter the contents of the tweet : ")
            tweet = tweet.strip()

            likes = input("Enter the number of likes this tweet has : ")
            likes = likes.strip()

            date = input("Enter the date that this tweet was created as YYYY-MM-DD : ")
            date = date.strip()

            hashtag = input("Enter the hashtag for this tweet (Donald Trump/Joe Biden)")
            hashtag = hashtag.replace(" ", "")

            user_id = self.get_user_id(username)  # need user_id use username to get it
            tweet_id = 1 + self.get_new_tweet_id()  # tweet id is number of tweets in table + 1

            query = "insert into tweet (id, user_id, created_at, tweet, likes, hashtag) \n"
            query += "values({0}, {1}, \"{2}\", \"{3}\", {4}, \"{5}\");".format(tweet_id, user_id, date, tweet,
                                                                                    likes, hashtag)
            results = self.execute_query(query, True)

        #add_annotation_county command : Add annotation to a specific county's party
        def add_annotation_county(self):

            if self.check_annotation_column() == False:
                self.add_annotation_column()

            county = input("Enter the county to add/modify the annotation for : ")
            county = county.strip()
            state = input("Enter the state that {0} belongs to : ".format(county))
            state = state.strip()

            query = "select candidate, party, annotations from county_candidate_statistics where " \
                    "county_id = (select id from county where name=\"{0}\" and state_id = ( select id from state " \
                    "where name = \"{1}\"));".format(county, state)

            # print(query)
            results = self.execute_query(query, False)

            if len(results) == 0:
                print("No results to show. State/County pairing does not exist.")
                return

            counter = 1
            print("Poll Rankings in {0} are as follows : ".format(county))

            for result in results:
                print("Candidate Name : {0:20} Party Affiliation : {1:4} Annotations : {2}".format(result[0], result[1],
                                                                                                result[2]))
                counter += 1

            party = input("Enter the party that you wish to add an annotation for : ")
            annotation = input("Enter the annotation that you wish to add : ")

            query = "update county_candidate_statistics \n"
            query += "set annotations = \"{0}\" where party = \"{1}\" and county_id = (select id from county where name=\"{2}\" and state_id = ( select id from state " \
                     "where name = \"{3}\"));".format(annotation, party, county, state)

            # print(query)
            result = self.execute_query(query, True)

        # view_annotation_county : View annotations for all parties in an associated county
        def view_annotation_county(self):

            if self.check_annotation_column() == False:

                print("No annotations created")
                return
            else:

                county = input("Enter the county that you wish to view the annotation for : ")
                county = county.strip()
                state = input("Enter the state that {0} belongs to : ".format(county))
                state = state.strip()

                query = "select candidate, party, annotations from county_candidate_statistics where county_id = (select id from county where name = \"{0}\" and state_id = (select id from state where name = \"{1}\"));".format(
                    county, state)

                result = self.execute_query(query, False)

                if len(result) == 0:
                    print("No results to display for county {0} in the state of {1}".format(county, state))
                    return

                for row in result:
                    print("Candidate Name : {0:20} Party : {1:4} Annotations : {2}".format(row[0], row[1], row[2]))

        # Help command - Prints out all available commands and their descriptions
        def display_command_list(self):

            stored = ("Command Name", "Command Description")
            print("{0:25} - {1}".format(stored[0], stored[1]))

            for command, description in zip(self.command_list, self.command_description):
                print("{0:25} - {1}".format(command, description))


        #HELPER FUNCTIONS - functions that the command functions call to simplify logic

        #returns a list of columns from a given table
        def get_column(self, table_name: str):

            cursor.execute("Show columns from {0}".format(table_name))
            results = cursor.fetchall()
            fields = []  # field name

            for result in results:
                fields.append(result[0])

            return fields

        def setup_stateid_table(self):

            query = "select id, name from state;"

            results = self.execute_query(query, False)
            state_id = {}

            for row in results:
                state_id[row[0]] = row[1]

            return state_id

        # Used to limit results of query
        def limit_results(self):

            num = input("Maximum results to be returned : ")
            return "LIMIT {0}".format(num)

        # Executes query and prints out if there is an error

        def execute_query(self, query: str, condition : bool):

            try:
                cursor.execute(query)
                results = cursor.fetchall()
                if condition:
                    print("Operation successful")
                return results

            except mysql.Error as err:

                print("Query unsuccessful. Please check your inputs")

        # Helper function for group_county_by
        # Some categories have a space in them. Converts spacing to database appropriate names
        def sanitize_categories(self, input: str):

            input = input.lower()

            if input =="voting age":
                return "voting_age"
            elif input =="income per capita":
                return "income_per_cap"
            elif input == "child poverty":
                return "child_poverty"
            elif input == "work from home":
                return "work_from_home"
            elif input == "private work":
                return "private_work"
            elif input == "public work":
                return "public_work"
            elif input == "self employed":
                return "self_employed"
            elif input == "family work":
                return "family_work"
            else:
                return input # leave as is


        # Gets user_id from a given username
        def get_user_id(self, user_name):

            query = "select id from user where display_name = \"{0}\"".format(user_name)
            result =  self.execute_query(query, False)
            for row in result:
                return row[0]

        # Generates a new user_id by returning the maximum current id
        def get_new_user_id(self):

            result = self.execute_query("select max(id) from user;", False)
            for row in result:
                return row[0]
        # Generates a new tweet_id by returning the maximum current id
        def get_new_tweet_id(self):

            result = self.execute_query("select max(id) from tweet;", False)
            for row in result:
                return row[0]

        # Add annotation column
        def add_annotation_column(self):

            query = "alter table county_candidate_statistics \n"
            query += "add column annotations varchar(255);"
            result = self.execute_query(query, False)
            db.commit()

            return

        # Check if we already added annotations column
        def check_annotation_column(self):

            columns = self.get_column("county_candidate_statistics")
            annotate = "annotations"
            return annotate in columns



        def get_county_id(self, county:str, state:str):

            query = "select id from county where  name = \"{0}\" and state_id = (select id from state where name = \"{1}\");".format(county,state)

            # print(query)
            results = self.execute_query(query, False)

            for result in results:
                return result[0]

if __name__ == "__main__":

    dotenv.load_dotenv(dotenv.find_dotenv())
    username = os.environ.get("DB_USER")
    passphrase = os.environ.get("DB_PASS")
    dbname = os.environ.get("DB_NAME")

    try:
        db = mysql.connect(

            host="marmoset04.shoshin.uwaterloo.ca",
            user=username,
            password=passphrase,
            database=dbname
        )
    except mysql.Error as err:

        if err.errno == mysql.errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")

        elif err.errno == mysql.errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")

        else:
            print(err)

    cursor = db.cursor()
    client = Database()

    while True:

        db.commit()
        command = input("Please select a command : ")
        command = command.lower() # No longer case sensitive
        command = command.strip()


        if command == "quit": # Works

            break

        elif command == "help":  # Works

            client.display_command_list()

        elif command == "state_votes": # Works

            client.get_votes_state()

        elif command == "find_user": # Works

            client.find_user()

        elif command == "county_results":  # Works

            client.results_county()

        elif command == "group_county_by": # Works

            client.group_county_by()

        elif command == "get_tweet_candidate": # Untested - Should work

            client.get_tweet_candidate()

        elif command == "count_tweet_region": # Untested - Should work

            client.count_tweet_region()

        elif command == "update_poll_count": # Works

            client.update_poll_count()

        elif command == "update_state_count": # Works

            client.update_state_count()

        elif command == "insert_tweet": # Works

            client.insert_tweet()

        elif command == "add_annotation_county": # Works

            client.add_annotation_county()

        elif command == "view_annotation_county": # Works

            client.view_annotation_county()

        elif command == "add_user":

            client.add_user()


        else:

            print("Invalid command, please enter a valid command!")

    db.close()