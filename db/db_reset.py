import dotenv
import os

import mysql.connector as mysql

# drop_tables drops all the tables
def drop_tables(db):
    with open('db/drop_tables.sql', 'r') as f, db.cursor() as cursor:
        results = cursor.execute(f.read(), multi=True)
        for r in results:
            print("Running query: ", r)
        db.commit()

if __name__ == "__main__":
    dotenv.load_dotenv(dotenv.find_dotenv())
    username = os.environ.get("DB_USER")
    passphrase = os.environ.get("DB_PASS")
    dbname = os.environ.get("DB_NAME")

    db = mysql.connect(
        host="marmoset04.shoshin.uwaterloo.ca",
        user=username,
        password=passphrase,
        database=dbname,
    )

    drop_tables(db)
    print('db reset complete')