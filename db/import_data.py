import csv
import os
import os
import dotenv

import mysql.connector as mysql

CSV_BASE_PATH = 'data'
COUNTY_STATISTICS_PATH = f'{CSV_BASE_PATH}/county_statistics.csv'
PRESIDENT_COUNTY_PATH = f'{CSV_BASE_PATH}/president_county.csv'
PRESIDENT_COUNTY_CANDIDATE_PATH = f'{CSV_BASE_PATH}/president_county_candidate.csv'
PRESIDENT_STATE_PATH = f'{CSV_BASE_PATH}/president_state.csv'
TRUMP_BIDEN_POLLS_PATH = f'{CSV_BASE_PATH}/trump_biden_polls.csv'
HASHTAG_TRUMP_PATH = f'{CSV_BASE_PATH}/hashtag_donaldtrump.csv'
HASHTAG_BIDEN_PATH = f'{CSV_BASE_PATH}/hashtag_joebiden.csv'

COUNTY_STATISTICS_PROCESSED_PATH = f'{CSV_BASE_PATH}/county_statistics_processed.csv'
PRESIDENT_COUNTY_CANDIDATE_PROCESSED_PATH = f'{CSV_BASE_PATH}/president_county_candidate_processed.csv'
PRESIDENT_STATE_PROCESSED_PATH = f'{CSV_BASE_PATH}/president_state_processed.csv'
TRUMP_BIDEN_POLLS_PROCESSED_PATH= f'{CSV_BASE_PATH}/trump_biden_polls_processed.csv'
TWEETS_PROCESSED_PATH = f'{CSV_BASE_PATH}/tweets_processed.csv'
USERS_PROCESSED_PATH = f'{CSV_BASE_PATH}/users_processed.csv'

BATCH_SIZE = 50

STATE_ABBREVIATIONS = {
    'AL':   'ALABAMA',        
    'AK':   'ALASKA',
    'AZ':   'ARIZONA',
    'AR':   'ARKANSAS',
    'CA':   'CALIFORNIA',
    'CO':   'COLORADO',
    'CT':   'CONNECTICUT',
    'DE':   'DELAWARE',
    'DC':   'DISTRICT OF COLUMBIA',
    'FL':   'FLORIDA',
    'GA':   'GEORGIA',
    'GU':   'GUAM',
    'HI':   'HAWAII',
    'ID':   'IDAHO',
    'IL':   'ILLINOIS',
    'IN':   'INDIANA',
    'IA':   'IOWA',
    'KS':   'KANSAS',
    'KY':   'KENTUCKY',
    'LA':   'LOUISIANA',
    'ME':   'MAINE',
    'MD':   'MARYLAND',
    'MA':   'MASSACHUSETTS',
    'MI':   'MICHIGAN',
    'MN':   'MINNESOTA',
    'MS':   'MISSISSIPPI',
    'MO':   'MISSOURI',
    'MT':   'MONTANA',
    'NE':   'NEBRASKA',
    'NV':   'NEVADA',
    'NH':   'NEW HAMPSHIRE',
    'NJ':   'NEW JERSEY',
    'NM':   'NEW MEXICO',
    'NY':   'NEW YORK',
    'NC':   'NORTH CAROLINA',
    'ND':   'NORTH DAKOTA',
    'MP':   'NORTHERN MARIANA IS',
    'OH':   'OHIO',
    'OK':   'OKLAHOMA',
    'OR':   'OREGON',
    'PA':   'PENNSYLVANIA',
    'PR':   'PUERTO RICO',
    'RI':   'RHODE ISLAND',
    'SC':   'SOUTH CAROLINA',
    'SD':   'SOUTH DAKOTA',
    'TN':   'TENNESSEE',
    'TX':   'TEXAS',
    'UT':   'UTAH',
    'VT':   'VERMONT',
    'VA':   'VIRGINIA',
    'VI':   'VIRGIN ISLANDS',
    'WA':   'WASHINGTON',
    'WV':   'WEST VIRGINIA',
    'WI':   'WISCONSIN',
    'WY':   'WYOMING',
}

class County:
    def __init__(self, id, latitude, longitude):
        self.id = id
        self.lat = latitude
        self.long = longitude

states = {} # states stores the mapping: state-name -> state_id
counties = {} # counties stores the mapping: {state-name}_{county-name} -> County(id, lat, long)
users = {} # users maps user_id -> bool

def parse_tweet_csv(raw_path, hashtag, processed_tweets_path, processed_users_path):
    with open(raw_path, encoding='utf-8') as raw_file, open(processed_tweets_path, 'a', newline='', encoding='utf-8') as processed_tweets_file, open(processed_users_path, 'w', newline='', encoding='utf-8') as processed_users_file:
        # skip the first row since it only includes column names
        next(raw_file)
        csv_reader = csv.reader(raw_file)
        tweet_writer = csv.writer(processed_tweets_file)
        user_writer = csv.writer(processed_users_file)
        idx = 0
        user_idx = 0
        errors = 0
        for row in csv_reader:
            if idx > 50000: # limit the amount of tweets being imported as inserting into the database is very slow
                break
            try:
                idx += 1
                user_id = row[6] 
                tweet_writer.writerow([idx, user_idx, row[0], row[2], hashtag, row[3], row[4], row[5]])
                if not user_id in users:
                    user_idx += 1
                    users[user_id] = True
                    county_id = None
                    # find user's county
                    latitude = float(row[13])
                    longitude = float(row[14])
                    for county in counties.values():
                        if round(latitude, 0) == county.lat and round(longitude, 0) == county.long:
                            county_id = county.id
                            break
                    
                    user_writer.writerow([user_idx, county_id, row[7], row[8], row[9], row[10], row[11]])
            except Exception:
                # lots of corrupted data in csvs so fail silently
                errors += 1
                pass
        print(f'tweets: done parsing {idx} rows, {errors} errors.')


def parse_csvs():
    # delete all *_processed.csv files
    if os.path.exists(PRESIDENT_COUNTY_CANDIDATE_PROCESSED_PATH):
        os.remove(PRESIDENT_COUNTY_CANDIDATE_PROCESSED_PATH)
    if os.path.exists(COUNTY_STATISTICS_PROCESSED_PATH):
        os.remove(COUNTY_STATISTICS_PROCESSED_PATH)
    if os.path.exists(PRESIDENT_STATE_PROCESSED_PATH):
        os.remove(PRESIDENT_STATE_PROCESSED_PATH)
    if os.path.exists(TRUMP_BIDEN_POLLS_PROCESSED_PATH):
        os.remove(TRUMP_BIDEN_POLLS_PROCESSED_PATH)
    if os.path.exists(TWEETS_PROCESSED_PATH):
        os.remove(TWEETS_PROCESSED_PATH)
    if os.path.exists(USERS_PROCESSED_PATH):
        os.remove(USERS_PROCESSED_PATH)

    # create mapping of state to state_id and create president_state_processed.csv
    with open(PRESIDENT_STATE_PATH) as raw_file, open(PRESIDENT_STATE_PROCESSED_PATH, 'x', newline='') as processed_file:
        # skip the first row since it only includes column names
        next(raw_file)
        csv_reader = csv.reader(raw_file)
        csv_writer = csv.writer(processed_file)
        idx = 0
        for row in csv_reader:
            idx += 1
            states[row[0].lower()] = idx
            csv_writer.writerow([idx, row[0], row[1]])

    # create mapping of county to county_id and create president_county_processed.csv
    with open(COUNTY_STATISTICS_PATH) as raw_file, open(COUNTY_STATISTICS_PROCESSED_PATH, 'x', newline='') as processed_file:
        # skip the first row since it only includes column names
        next(raw_file)
        csv_reader = csv.reader(raw_file)
        csv_writer = csv.writer(processed_file)
        idx = 0
        for row in csv_reader:
            idx += 1
            state_name = STATE_ABBREVIATIONS[row[2]]
            key = f'{state_name}_{row[1]}'.lower() # unique key in the format of 'state_county'
            latitude = None
            longitude = None
            try:
                latitude = round(float(row[13]), 0)
                longitude = round(float(row[14]), 0)
            except:
                pass
            counties[key] = County(idx, latitude, longitude)
            state_id = states[state_name.lower()]
            csv_writer.writerow([idx, state_id, row[1], row[13], row[14], row[15], row[16],row[17],row[18],row[19],
                row[20], row[21], row[22], row[23], row[24], row[25], row[26], row[27], row[28], row[29], row[30], row[31],
                row[32], row[33], row[34], row[35], row[36], row[37], row[38], row[39], row[40], row[41], row[42], row[43],
                row[44], row[45], row[46], row[47], row[48], row[49], row[50]])

    # create mapping of county to county_id and create president_county_processed.csv
    with open(PRESIDENT_COUNTY_CANDIDATE_PATH) as raw_file, open(PRESIDENT_COUNTY_CANDIDATE_PROCESSED_PATH, 'x', newline='') as processed_file:
        # skip the first row since it only includes column names
        next(raw_file)
        csv_reader = csv.reader(raw_file)
        csv_writer = csv.writer(processed_file)
        idx = 0
        for row in csv_reader:
            idx += 1
            county_name = row[1].replace(' County', '').replace(' Parish', '')
            key = f'{row[0]}_{county_name}'.lower() # unique key in the format of 'state_county'
            county_id = counties[key].id
            csv_writer.writerow([idx, county_id, row[2], row[3], row[4], row[5]])

    # create trump_biden_processed.csv
    with open(TRUMP_BIDEN_POLLS_PATH) as raw_file, open(TRUMP_BIDEN_POLLS_PROCESSED_PATH, 'x', newline='') as processed_file:
        # skip the first row since it only includes column names
        next(raw_file)
        csv_reader = csv.reader(raw_file)
        csv_writer = csv.writer(processed_file)
        idx = 0
        for row in csv_reader:
            idx += 1
            key = f'{row[0]}_{county_name}'.lower() # unique key in the format of 'state_county'
            state_id = row[3]
            csv_writer.writerow([idx, state_id, row[5], row[2], row[7], row[8], row[11], row[12], row[13], row[15], row[19],
                row[20], row[21], row[22], row[23], row[24], row[25], row[28], row[30], row[35], row[36], row[37]])
    
    # create users_processed.csv and tweets_processed.csv
    parse_tweet_csv(HASHTAG_BIDEN_PATH, 'JoeBiden', TWEETS_PROCESSED_PATH, USERS_PROCESSED_PATH)
    parse_tweet_csv(HASHTAG_TRUMP_PATH, 'DonaldTrump', TWEETS_PROCESSED_PATH, USERS_PROCESSED_PATH)


def import_data(db):    
    cursor = db.cursor()

    with open(PRESIDENT_STATE_PROCESSED_PATH) as csvfile:
        values = []
        csv_reader = csv.reader(csvfile)
        file_len = sum(1 for row in csv_reader)
        csvfile.seek(0) # go back to top of file
        idx = 1
        q = 'INSERT IGNORE INTO state VALUES(%s, %s, %s)'
        rows = 0
        for row in csv_reader:
            values.append([None if x == '' else x for x in row])
            if idx % BATCH_SIZE == 0 or idx == file_len:
                rows += len(values)
                cursor.executemany(q, values)
                db.commit()
                values = []
            idx += 1
        print(f'inserted {rows} rows into state table')

    with open(COUNTY_STATISTICS_PROCESSED_PATH) as csvfile:
        values = []
        csv_reader = csv.reader(csvfile)
        file_len = sum(1 for row in csv_reader)
        csvfile.seek(0) # go back to top of file
        idx = 1
        q = 'INSERT IGNORE INTO county VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
        rows = 0
        for row in csv_reader:
            values.append(tuple([None if x == '' else x for x in row]))
            if idx % BATCH_SIZE == 0 or idx == file_len:
                rows += len(values)
                cursor.executemany(q, values)
                db.commit()
                values = []
            idx += 1
        print(f'inserted {rows} rows into county table')
    
    with open(PRESIDENT_COUNTY_CANDIDATE_PROCESSED_PATH) as csvfile:
        values = []
        csv_reader = csv.reader(csvfile)
        file_len = sum(1 for row in csv_reader)
        csvfile.seek(0) # go back to top of file
        idx = 1
        q = 'INSERT IGNORE INTO county_candidate_statistics VALUES(%s, %s, %s, %s, %s, %s)'
        rows = 0
        for row in csv_reader:
            values.append(tuple([None if x == '' else x for x in row]))
            if idx % BATCH_SIZE == 0 or idx == file_len:
                rows += len(values)
                cursor.executemany(q, values)
                db.commit()
                values = []
            idx += 1
        print(f'inserted {rows} rows into county_candidate_statistics table')
    

    with open(USERS_PROCESSED_PATH, encoding='utf-8') as csvfile:
        values = []
        csv_reader = csv.reader(csvfile)
        file_len = sum(1 for row in csv_reader)
        csvfile.seek(0) # go back to top of file
        idx = 1
        q = 'INSERT IGNORE INTO user VALUES(%s, %s, %s, %s, %s, %s, %s)'
        rows = 0
        for row in csv_reader:
            values.append(tuple([None if x == '' else x for x in row]))
            if idx % BATCH_SIZE == 0 or idx == file_len:
                rows += len(values)
                cursor.executemany(q, values)
                db.commit()
                values = []
            idx += 1
        print(f'inserted {rows} rows into user table')

    with open(TWEETS_PROCESSED_PATH, encoding='utf-8') as csvfile:
        values = []
        csv_reader = csv.reader(csvfile)
        file_len = sum(1 for row in csv_reader)
        csvfile.seek(0) # go back to top of file
        idx = 1
        q = 'INSERT IGNORE INTO tweet VALUES(%s, %s, %s, %s, %s, %s, %s, %s)'
        rows = 0
        for row in csv_reader:
            values.append(tuple([None if x == '' else x for x in row]))
            if idx % BATCH_SIZE == 0 or idx == file_len:
                rows += len(values)
                cursor.executemany(q, values)
                db.commit()
                values = []
            idx += 1
        print(f'inserted {rows} rows into tweet table')
    db.close()
