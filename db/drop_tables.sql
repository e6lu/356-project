drop table if exists tweet;
drop table if exists user;
drop table if exists county_candidate_statistics;
drop table if exists county;
drop table if exists state;