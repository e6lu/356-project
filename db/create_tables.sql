create table user (
    id int,
    county_id int,
    display_name varchar(50),
    url_name varchar(50),
    description varchar(50),
    join_date timestamp,
    follower_count int,
    primary key (id)
);

create table tweet (
    id bigint,
    user_id int,
    created_at timestamp,
    tweet varchar(280),
    hashtag varchar(12),
    likes int,
    retweets int,
    source varchar(50),
    primary key (id),
    foreign key (user_id) references user(id)
);

create table state (
    id int,
    name varchar(50),
    total_votes int,
    primary key (id)
);

create table county (
    id int,
    state_id int,
    name varchar(50),
    latitude decimal(8),
    longitude decimal(8),
    cases int,
    deaths int,
    population int,
    men int,
    women int,
    hispanic decimal(4,2),
    white decimal(4,2),
    black decimal(4,2),
    native decimal(4,2),
    asian decimal(4,2),
    pacific decimal(4,2),
    voting_age int,
    income int,
    income_err int,
    income_per_cap int,
    income_per_cap_err int,
    poverty decimal(4,2),
    child_poverty decimal(4,2),
    professional decimal(4,2),
    service decimal(4,2),
    office decimal(4,2),
    construction decimal(4,2),
    production decimal(4,2),
    drive decimal(4,2),
    carpool decimal(4,2),
    transit decimal(4,2),
    walk decimal(4,2),
    other_transport decimal(4,2),
    work_from_home decimal(4,2),
    total_commute decimal(4,2),
    employed int,
    private_work decimal(4,2),
    public_work decimal(4,2),
    self_employed decimal(4,2),
    family_work decimal(4,2),
    unemployed decimal(4,2),
    primary key (id),
    foreign key (state_id) references state(id)
);

create table county_candidate_statistics (
    id int,
    county_id int,
    candidate varchar(20),
    party varchar(3),
    votes int,
    won boolean,
    primary key (id),
    foreign key (county_id) references county(id)
);

alter table county_candidate_statistics add index party_idx (party);
alter table county_candidate_statistics add index candidate_idx (candidate);
alter table county_candidate_statistics add index votes_idx (votes);
