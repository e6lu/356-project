from import_data import import_data, parse_csvs
import dotenv
import os

import mysql.connector as mysql

# create_tables creates all the tables and indexes in the schema
def create_tables(db):
    with open('db/create_tables.sql', 'r') as f, db.cursor() as cursor:
        results = cursor.execute(f.read(), multi=True)
        for r in results:
            print("Running query: ", r)
    db.commit()

if __name__ == "__main__":
    dotenv.load_dotenv(dotenv.find_dotenv())
    username = os.environ.get("DB_USER")
    passphrase = os.environ.get("DB_PASS")
    dbname = os.environ.get("DB_NAME")

    db = mysql.connect(
        host="marmoset04.shoshin.uwaterloo.ca",
        user=username,
        password=passphrase,
        database=dbname
    )

    create_tables(db)
    print('done creating tables')

    parse_csvs()
    print('done parsing csvs')

    import_data(db)
    print('db initialization complete')